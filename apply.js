const declareFunc = (num, func, args) => {
    if (num === 0) {
        return args;
    } 
    return declareFunc(num-1, func, func(args));
}
const apply = (num, func, args) => ((num === 0) ? args : declareFunc (num-1, func, func(args)));
export default apply; 