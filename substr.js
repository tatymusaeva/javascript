'use strict'

const substr = (string, index=0, length=string.length) => {
    if(string === ''){
        return '';
    }

    if(length < 0){
        length = 1;
    }

    if(index < 0){
        index = 0;
    }
   
    if(length > index + length){
        length = string.length - index;
    }

    return string.substr(index, length);
  
}