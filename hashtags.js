/**
 * @param {String} tweet
 * @returns {String[]}
 */
module.exports = function (tweet) {
    var arrOfStrings = tweet.split(' '),
        arrHashTags = [];

    var arrHashTags = arrOfStrings.filter(
        function getStrWithHashes( item ){
            if ( item.indexOf( '#' ) != -1 ) {
                return item;
            }
        }
    ).map(
        function delHashesWithPunctuation( item ){
            if ( item.indexOf( '#' ) != -1 ) {
                var ind = item.indexOf( '#' ),
                    hashTag = item.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g,'');
                return hashTag;
            }
        }
    );
    return arrHashTags;
};
