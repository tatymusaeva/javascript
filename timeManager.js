/**
 * @param {String} date
 * @returns {Object}
 */
module.exports = function(date){
    var objTimeCntrl = {
        date: new Date(date),
        units: ['years','months', 'days', 'hours', 'minutes'],
        add: function(arg1, arg2){
            var validUnit = this.units.filter(function(str) { return str === arg2 });

            if(arg1 < 0 || validUnit.length != 1){
                throw TypeError('Переданы не верные аргументы');
            }

            switch(arg2){
                case 'years':
                    this.date.setFullYear(this.date.getFullYear() + arg1);
                    break;
                case 'months':
                    this.date.setMonth(this.date.getMonth() + arg1);
                    break;
                case 'days':
                    this.date.setDate(this.date.getDate() + arg1);
                    break;
                case 'hours':
                    this.date.setHours(this.date.getHours() + arg1);
                    break;
                case 'minutes':
                    this.date.setMinutes(this.date.getMinutes() + arg1);
                    break;
                default:
                    console.log('');
            }

            return this;
        },
        subtract: function(arg1, arg2){
            var validUnit = this.units.filter(function(str) { return str === arg2 });

            if(arg1 < 0 || validUnit.length != 1){
                throw TypeError('Переданы не верные аргументы');
            }

            switch(arg2){
                case 'years':
                    this.date.setFullYear(this.date.getFullYear() - arg1);
                    break;
                case 'months':
                    this.date.setMonth(this.date.getMonth() - arg1);
                    break;
                case 'days':
                    this.date.setDate(this.date.getDate() - arg1);
                    break;
                case 'hours':
                    this.date.setHours(this.date.getHours() - arg1);
                    break;
                case 'minutes':
                    this.date.setMinutes(this.date.getMinutes() - arg1);
                    break;
                default:
                    console.log('');
            }

            return this;
        },
        addLeadingZero: function(val){
            var val = String(val);
            return val.length < 2 ? '0' + val : val;
        }
    };
    Object.defineProperty(objTimeCntrl, 'value', {
        get: function() {
            var year = this.date.getFullYear(),
                month = this.date.getMonth() + 1,
                day = this.date.getDate(),
                hour = this.date.getHours(),
                minutes = this.date.getMinutes();
                
            resultDate = year + '-' + this.addLeadingZero(month) + 
                        '-' + this.addLeadingZero(day) + ' ' + 
                        this.addLeadingZero(hour) + ':' + 
                        this.addLeadingZero(minutes);
            
            return resultDate;
        },
        enumerable: true
    });

    return objTimeCntrl;
}