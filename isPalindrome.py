def is_palindrome(str):
    if(len(str) <= 1):
        return True

    first_char = str[0]
    last_char = str[-1]

    if(first_char != last_char):
        return False

    str_res = str[1:-1]

    return is_palindrome(str_res)