// Телефонная книга
var phoneBook = {};

/**
 * @param {String} command
 * @returns {*} - результат зависит от команды
 */
module.exports = function (command) {
    var argStr = command.split( /[ ,]+/ ),
        commandsName = argStr[0];

    if( commandsName == 'ADD' ){
        var phonesName = argStr[1],
            phonesNumber = argStr.slice(2);
        if( !phoneBook.hasOwnProperty(phonesName) ) {
            phoneBook[phonesName] = phonesNumber;
        } else {
            phoneBook[phonesName] =  phoneBook[phonesName].concat(phonesNumber);
            if( Object.keys(phoneBook).length > 1 ) {
                for ( var i = 0; i < Object.keys(phoneBook).length; i++ ){
                    var key = Object.keys(phoneBook)[i];
                    phoneBook[key] = phoneBook[key];
                }
            }
        }
    }

    if( commandsName == 'REMOVE_PHONE' ){
        var phoneNumb = argStr[1];
        for ( var property in phoneBook ) {
            if ( phoneBook.hasOwnProperty( property ) ) {
                if( phoneBook[property].indexOf( argStr[1] ) != -1 ){
                    var flag = phoneBook[property].indexOf( argStr[1] );
                    phoneBook[property].splice(flag, 1);
                    return true;
                }
            }
        }
        return false;
    }

    if( commandsName == 'SHOW' ){
        var list = [];
        Object.keys( phoneBook ).forEach( function( key ) {
            if( phoneBook[key].length != 0 ){
                var phones = String(phoneBook[key]).replace(/,(?=[^\s])/g, ', ');
                list.push( key + ': ' + phones );
            }
        });
        return list.sort();
    }
};
