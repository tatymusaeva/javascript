/**
 * @param {Array} collection
 * @params {Function[]} – Функции для запроса
 * @returns {Array}
 */
var fSelect = 'select',
    fFilterIn = 'filterIn';

function query(collection) {
    if(collection === undefined) {
        return;
    }
    if(collection === null) {
        return null;
    }
    if(!Array.isArray(collection)) {
        return collection;
    }
    var res;

    if(arguments.length === 1) {
        res = collection.slice(0);
    }
    else {
        var argsAction = Array.prototype.slice.call(arguments),
            actions = argsAction.slice(1),
            fCollection = funcCollection(collection, actions);

        res = funcSelectProperties(fCollection, concatPropertyFromSelectActions(actions));
    }
    return res;
}

function select() {
    var res = {
        action: fSelect,
        propNames: Array.prototype.slice.call(arguments)
    };
    return res;
}

function filterIn(prop, values) {
    
    var res = {
        action: fFilterIn,
        propName: prop,
        values: values
    };
    return res;
}

function funcCollection(items, actions) {
    var filterActions = actions.filter(isFilterInAction);
    var res = items.filter(
        function(currItem){
            var resFilterAction = true;
            filterActions.forEach(
                function(currFilterAction){
                    if(resFilterAction) { 
                        if(currItem.hasOwnProperty(currFilterAction.propName)) { 
                            resFilterAction = currFilterAction.values.indexOf(currItem[currFilterAction.propName]) !== -1;
                        }
                        else {
                            resFilterAction = false;
                        }
                    }
                }
            );
            return resFilterAction;
        }
    );
    return res;
}

function isFilterInAction(action) {
    return action !== undefined && action !== null && action.action === fFilterIn;
}

function concatPropertyFromSelectActions(actions) {
    return actions.reduce(
        function(res, currAction){
            if(currAction !== undefined && currAction !== null && currAction.action === fSelect) {
                if(res === null) {
                    res = currAction.propNames;
                }
                else {
                    res = result.filter(
                        function(currPropertyName){
                            return currAction.propNames.indexOf(currPropertyName) !== -1;
                        }
                    );
                }
            }
            return res;
        },
        null
    );
}

function funcSelectProperties(items, propNames) {
    res = items.map(function(currItem){
        var resItem = {};
        if(propNames !== undefined && propNames !== null) {
            propNames.forEach(function(propName) {
                if(currItem.hasOwnProperty(propName)) {
                    resItem[propName] = currItem[propName];
                }
            });
        }
        return resItem;
    });
    return res;
}

module.exports = {
    query: query,
    select: select,
    filterIn: filterIn
};