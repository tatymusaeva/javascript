const reverse = (str) => {
    const lastIndex = str.length - 1;
    const iter = (counter, acc) => {
        if(counter < str.length){
            acc += str[lastIndex - counter];
            return iter(counter + 1, acc);
        }
        return acc;
    }
    return iter(0, '');
}
let t = reverse('orange');
console.log(t);