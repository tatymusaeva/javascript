const isPalindrome = (str) => {
    if(str.length <= 1){
        return true;
    }

    let firstChar = str.charAt(0),
        lastChar = str.charAt(str.length - 1);

    if(firstChar !== lastChar){
        return false;
    }

    const strRes = str.substr(1, str.length - 2);

    return isPalindrome(strRes);
};

export default isPalindrome;